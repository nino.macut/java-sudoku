package vub.sudoku;
import java.util.Arrays;
import java.util.Random;

public class Game {
    private int[][] board;

    public Game(){
        generateRandomBoard(9);
    }

    public Game(int size){
        generateRandomBoard(size);
    }

    public Game(int[][] board){
        this.board = board;
    }

    private boolean isInputValid(int row, int column, int number){
        int boardLength = board.length;

        //Provjerava postoji li uneseni broj u istom redu.
        for (int columnSweep = 0; columnSweep < boardLength; columnSweep++){
            if (board[row][columnSweep] == number){
                return false; //Vrati false ako je uneseni broj pronađen u redu.
            }
        }

        //Provjerava postoji li uneseni broj u istom stupcu.
        for (int rowSweep = 0; rowSweep < boardLength; rowSweep++){
            if (board[rowSweep][column] == number){
                return false; //Vrati false ako je uneseni broj pronađen u stupcu.
            }
        }

        //Provjerava postoji li uneseni broj u istoj "kutiji".
        int root = (int) Math.sqrt(boardLength); //Vraća korijen veličine ploče. Primjer: Ploča je 9x9, stoga su kocke 3x3.
        int boxRowStart = row - row % root; //Primjer: Unesli smo 5; 5 - 5 % 3 = 3. Redovi kreću od 4. reda (0,1,2,[3]).
        int boxColumnStart = column - column % root; //Primjer: Unesli smo 6; 6 - 6 % 3 = 6. Stupci kreću od 7. stupca (0,1,2,3,4,5,[6]).

        for (int boxRowSweep = boxRowStart; boxRowSweep < boxRowStart + root; boxRowSweep++){
            for (int boxColumnSweep = boxColumnStart; boxColumnSweep < boxColumnStart + root; boxColumnSweep++){
                if (board[boxRowSweep][boxColumnSweep] == number){
                    return false; //Vrati false ako je uneseni broj pronađen u "kutiji".
                }
            }
        }

        //Ako prođu sve tri provjere vraća true.
        return true;
    }

    public void inputNumber(int row, int column, int number){
        if (isInputValid(row,column,number)){
            board[row][column] = number; //Ako su provjere prošle upiši broj u ploču.
        }
        else{
            //System.out.println("ERROR: Netočan unos!"); //Ako provjere nisu prošle, javi grešku.
        }
    }

    //Generira ploču koja je spremna za igru sa danim veličinama.
    public void generateRandomBoard(int size){
        double sq = Math.sqrt(size);

        if ((sq - Math.floor(sq)) == 0){
            this.board = generateEmptyBoard(size);

            Random random = new Random();
            boolean randomBoolean;

            for (int row = 0; row < size; row++){
                for(int column = 0; column < size; column++){
                    randomBoolean = random.nextBoolean();
                    if(randomBoolean){
                        int randomNumber = random.nextInt((size - 1) + 1) + 1;
                        inputNumber(row,column,randomNumber);
                    }
                }
            }
        }
        else{
            System.out.println("ERROR: Incorrect size!");
        }
    }

    //Generira praznu ploču sa danom veličinom.
    private int[][] generateEmptyBoard(int size){
        int[][] board = new int[size][size];
        for (int[] row : board){
            Arrays.fill(row, 0);
        }
        return board;
    }

    public void printBoard(){
        int boardLength = board.length;
        for (int row = 0; row < boardLength; row++){
            for (int column = 0; column < boardLength; column++){
                System.out.print(board[row][column] + " ");
            }
            System.out.print("\n");
        }
        System.out.print("\n\n");
    }
}
